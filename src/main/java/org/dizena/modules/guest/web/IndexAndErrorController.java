package org.dizena.modules.guest.web;

import org.dizena.common.bean.ResultVO;
import org.springframework.boot.web.servlet.error.ErrorAttributes;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.WebRequest;

import javax.annotation.Resource;

@Controller
public class IndexAndErrorController implements ErrorController {
    @Resource
    private ErrorAttributes errorAttributes;

    @GetMapping("/")
    public String index() {
        return "redirect:https://www.dizena.com";
    }

    @RequestMapping("/error")
    @ResponseBody
    public ResultVO error(WebRequest webRequest) {
        Throwable error = errorAttributes.getError(webRequest);
        String msg = null;
        if(error != null){
            msg = error.getMessage();
        }
        return new ResultVO(500, msg);
    }

}
