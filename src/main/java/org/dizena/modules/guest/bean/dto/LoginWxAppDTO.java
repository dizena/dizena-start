package org.dizena.modules.guest.bean.dto;

import lombok.Data;

@Data
public class LoginWxAppDTO {
    private String code;
    private String nickname;
    private String avatar;
    private String state;
    private String key;
}
