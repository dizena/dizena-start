package org.dizena.common.except;

import org.dizena.common.enums.ResultCode;

/**
 * 业务异常
 */
public class BizException extends BaseException {
    public BizException(ResultCode rc) {
        super(rc.code(), rc.msg());
    }
}
