package org.dizena.common.enums;

import lombok.AllArgsConstructor;

import java.util.Arrays;
import java.util.stream.Collectors;

@AllArgsConstructor
public enum ResultCode {
    SUCCESS(200, "ok"),
    ERROR(500, ""),

    /* 参数错误 1001-1999*/
    PARAM_IS_INVALID(1001, "参数无效"),

    Token_None(4000, "Token None"),
    Token_Error(4001, "Token Error"),
    Token_Login(4002, "Please Login"),

    CODE_ERROR(4011, "验证码错误"),
    CODE_EXPIRED(4012, "验证码过期"),
    LOGIN_Error(4013, "账户或密码错误"),
    USER_Locked(4014, "用户锁定"),
    USER_Cancel(4015, "用户注销"),
    USER_None(4016, "用户不存在"),
    USER_Exist(4017, "用户已经存在"),
    Role_Exist(4018, "角色已经存在"),
    Role_Keep(4019, "角色名保留不可用"),

    ROLE_AUTH_ERROR(5001, "角色权限错误"),
    Opt_Auth_None(5002, "没有操作权限"),
    Opt_Role_None(5003, "该角色无法执行"),
    ;

    private Integer code;
    private String msg;

    public Integer code() {
        return this.code;
    }

    public String msg() {
        return this.msg;
    }

    public static ResultCode byMsg(String msg) {
        return Arrays.stream(ResultCode.values()).filter(n -> n.msg.equals(msg)).collect(Collectors.toList()).get(0);
    }

}
